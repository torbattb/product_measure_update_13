# -*- coding: utf-8 -*-
{
    "name": "Product uom update v13",
    'summary': "Product uom update. UOM update. Update UOM. update product uom. update measure. update product measure. update unit of measure. update unit of product.",
    "version": "13.0",
    "description": """
        This module allows you to change product uom with any moves. Contact and support email: torbatj79@gmail.com
    """,
    "author": "Tb25",
    'support': 'torbatj79@gmail.com',
    "category": "Generic Modules",
    'price': 30.0,
    'currency': 'USD',
    "website": "",
    "license": "AGPL-3",
    'version': '13.0.1.0.0',
    'depends': ['product',
                'stock'],
    "update_xml": [
                'security/ir.model.access.csv',
                'views/product_uom_update_wizard.xml',
             ],
    'images': ['static/src/img/banner.png'],
    "auto_install": False,
    "installable": True,
    "application": True,
}
